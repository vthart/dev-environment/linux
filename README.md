# Development Environment Linux

This guide covers my peronal preferences and tools for a development environment on a new Linux environment. I have used this guides as references.
* http://linuxbrew.sh/
* https://dev.to/brpaz/my-linux-development-environment-of-2018-ch7
* https://itsfoss.com/best-ubuntu-apps/

Tested with ubuntu 18.10

## Vagrant 

To test this guide with vagrant:
```bash
vagrant init peru/ubuntu-18.10-desktop-amd64 \
  --box-version 20181202.01
vagrant up
vagrant ssh
```

## Linuxbrew

[Linuxbrew](http://linuxbrew.sh/) Linuxbrew is a fork of Homebrew, the macOS package manager, for Linux.
It can be installed in your home directory and does not require root access. The same package manager can be used on both your Linux server and your Mac laptop. Installing a modern version of glibc and gcc in your home directory on an old distribution of Linux takes five minutes.
Features, usage and installation instructions are summarised on the homepage. Terminology (e.g. the difference between a Cellar, Tap, Cask and so forth) is explained [here](https://docs.brew.sh/Formula-Cookbook#homebrew-terminology).

To install Linuxbrew run the following: terminal, hit Enter, and follow the steps on the screen:

The installation script installs Linuxbrew to /home/linuxbrew/.linuxbrew if possible and in your home directory at ~/.linuxbrew otherwise.

Paste at a Terminal prompt:
```bash
sh -c "$(curl -fsSL https://raw.githubusercontent.com/Linuxbrew/install/master/install.sh)"
```
- Install the Linuxbrew dependencies if you have sudo access:
  Debian, Ubuntu, etc.
    sudo apt-get install build-essential
  Fedora, Red Hat, CentOS, etc.
    sudo yum groupinstall 'Development Tools'
  See http://linuxbrew.sh/#dependencies for more information.
- Add Linuxbrew to your ~/.profile by running
    echo 'eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)' >>~/.profile
- Add Linuxbrew to your PATH
    PATH="/home/linuxbrew/.linuxbrew/bin:$PATH"
- We recommend that you install GCC by running:
    brew install gcc
- After modifying your shell profile, you may need to restart your session
  (logout and then log back in) if the brew command isn't found.
/usr/bin/which: no git in (/home/linuxbrew/.linuxbrew/Homebrew/Library/Homebrew/vendor/portable-ruby/current/bin:/home/vagrant/.local/bin:/home/vagrant/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin)
- Run `brew update --force` to complete installation by installing:
    /home/linuxbrew/.linuxbrew/share/doc/homebrew
    /home/linuxbrew/.linuxbrew/share/man/man1/brew.1
    /home/linuxbrew/.linuxbrew/share/zsh/site-functions/_brew
    /home/linuxbrew/.linuxbrew/etc/bash_completion.d/brew
    /home/linuxbrew/.linuxbrew/Homebrew/.git
- Run `brew help` to get started
- Further documentation: 
    https://docs.brew.sh
Warning: /home/linuxbrew/.linuxbrew/bin is not in your PATH.


Follow the Next steps instructions to add Linuxbrew to your PATH and to your bash shell profile script, either ~/.profile on Debian/Ubuntu or ~/.bash_profile on CentOS/Fedora/RedHat.
```bash
sudo apt-get install build-essential
test -d ~/.linuxbrew && eval $(~/.linuxbrew/bin/brew shellenv)
test -d /home/linuxbrew/.linuxbrew && eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)
test -r ~/.bash_profile && echo "eval \$($(brew --prefix)/bin/brew shellenv)" >>~/.bash_profile
echo "eval \$($(brew --prefix)/bin/brew shellenv)" >>~/.profile
```
You’re done! Try installing a package:
```bash
brew install hello
```
If you’re using an older distribution of Linux, installing your first package will also install a recent version of glibc and gcc.
```bash
brew install gcc
```

Use ```brew doctor``` to troubleshoot common issues.

Upgrade and cleanup packages:

```bash
brew update
brew upgrade
brew cleanup -s
```

This will:

* update the local base of available packages and versions, to know what is updatable
* installs new version of outdated packages
* removes outdated downloads and remove old versions of installed formulae
* allow you to keep only linked versions (by default, the last) and save some disk space

### Cask

[Cask](https://caskroom.github.io/) extends Homebrew and allows you to install large binary files via a command-line tool. 

Unfortunately casks is supported only on macOS

## Oh My ZSH

[Oh My Zsh](https://github.com/robbyrussell/oh-my-zsh/) is an open source, community-driven framework for managing your zsh configuration. It comes with a bunch of features out of the box and improves your terminal experience.

Install Oh My Zsh:

```bash
sudo apt-get install zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```

### Plugins
Add plugins to your shell by adding the name of the plugin to the plugin array in your .zshrc.

```
plugins=(git colored-man colorize pip python brew osx zsh-syntax-highlighting httpie docker vscode vagrant)
```

You'll find a list of all plugins on the [Oh My Zsh Wiki](https://github.com/robbyrussell/oh-my-zsh/wiki/Plugins).

### Themes
Changing theme is as simple as changing a string in your configuration file. The default theme is `robbyrussell`. Just change that value to change theme, and don't forget to apply your changes.

```
ZSH_THEME=pygmalion
```

You'll find a list of themes with screenshots on the [Oh My Zsh Wiki](https://github.com/robbyrussell/oh-my-zsh/wiki/Themes).

### Perl warnings and PATH
add exports to .zshrc
```
export PATH=PATH=$HOME/bin:/usr/local/bin:/home/linuxbrew/.linuxbrew/bin:$PATH
export LC_CTYPE=en_US.UTF-8
export LC_ALL=en_US.UTF-8
```

### Set zsh as default
```
chsh -s $(which zsh)
```

## Snapcraft

[Snapd](https://docs.snapcraft.io/getting-started/3876) a universal app store for Linux

```bash
$ sudo apt update
$ sudo apt install snapd
```

Add to /snap/bin to PATH in .zshrc
```
export PATH=PATH=$HOME/bin:/usr/local/bin:/home/linuxbrew/.linuxbrew/bin:/snap/bin:$PATH
```

### Search 

With browser go to [here](https://snapcraft.io/search?category=productivity&q=)

```
snap find "visual code"
Name                  Version            Publisher     Notes    Summary
vscode                1.29.1-1542309157  snapcrafters  classic  Code editing. Redefined.
code-insiders         1.30.0-1541692732  vscode✓       classic  Code editing. Redefined.
gitkraken             4.1.1              gitkraken✓    -        For repo management, in-app code editing & issue tracking.
cpp-dependencies      0.0.1              gocarlos      -        cpp-dependencies
atomify               2.1.2              ovilab        -        Atomify LAMMPS
epipolar-consistency  0.1                thahamsta     -        Consistency Conditions for any two X-ray images.
qxmledit              0.9.11             frederickjh   -        QXmlEdit is a simple XML editor based on Qt libraries.
demo-git              2.7.4-0ubuntu1     woodrow       -        fast, scalable, distributed revision control system
```

### Info 

```bash
snap info vlc
name:      vlc
summary:   The ultimate media player
publisher: VideoLAN✓
contact:   https://www.videolan.org/support/
description: |
  VLC is the VideoLAN project's media player.
  (...)
snap-id: RT9mcUhVsRYrDLG8qnvGiy26NKvv6Qkd
commands:
  - vlc
channels:
  stable:    3.0.0                   (158) 197MB -
  candidate: 3.0.0                   (158) 197MB -
  beta:      3.0.0-5-g407d4ba        (160) 197MB -
  edge:      4.0.0-dev-1218-g201542f (159) 197MB 

```

### Installing


Installing a snap is straightforward:
```bash
$ sudo snap install vlc
```

Channels are an important snap concept. They define which release of a snap is installed and tracked for updates. The stable channel is used by default, but opting to install from a different channel is easily accomplished:

```bash
$ sudo snap install --channel=edge vlc
```

After installation, the channel being tracked can be changed with:
```bash
$ sudo snap switch --channel=stable vlc
```

## Git

Let's install the latest version of Git:

```bash
sudo snap install git-ubuntu --classic
```

Next, we'll define your Git user:

```bash
git config --global user.name "Your Name Here"
git config --global user.email "your_email@youremail.com"
```

## SSH Config  

There 's no $HOME/.ssh config when you did not use SSH before. Let's create this by "ssh-ing" to a server (e.g. Github).

```bash
ssh github.com
```

Generate a new SSH key:

```bash
ssh-keygen -t rsa -C "your_email@example.com"
```

Run the following commands to add your SSH key to the ssh-agent.

```bash
eval "$(ssh-agent -s)"
```

No matter what operating system version you run you need to run this command to complete this step:

```bash
ssh-add -K ~/.ssh/id_rsa
```

## Visual Studio Code

Just started using Visual Studio Code instead of SublimeText. Not sure yet which is my favorite.

```bash
sudo snap install code-insiders --classic
```

## IntelliJ IDEA

This is my favorite IDE for development using Golang, Java or Javascript. If you're a student or an instructor (teaching staff members) all IDEs from JetBrains are free to use. You can read more on [their website](https://www.jetbrains.com/student/). If you're not a student they still offer a few free Community Edition (CE) IDEs.

Community Edition:

```bash
sudo snap install intellij-idea-community --classic
```

Ultimate Edition:

```bash
sudo snap install intellij-idea-ultimate --classic 
```

## Python

macOS, like Linux, ships with Python already installed. But you don't want to mess with the system Python (some system tools rely on it, etc.). 

[pyenv](https://github.com/yyuu/pyenv) is a Python version manager that can manage and install different versions of Python. 

```bash
brew install pyenv
```

You can install a specific Python version with:

```bash
pyenv install 3.7.1
```

Install pip for managing Python packages:

```bash
sudo apt install python3-pip
```

## Java

Install OpenJDK 11:

```bash
sudo apt install openjdk-11-jdk
```

## Golang

Go (also known as Golang) is an open source programming language maintained by Google.

```bash
brew install golang
```

## Vagrant

Create and configure lightweight, reproducible, and portable development environments. [Vagrant](http://www.vagrantup.com/) is a tool for managing virtual machines via a simple to use command line interface.

Vagrant uses Virtualbox to manage the virtual dependencies. 

```bash
sudo apt install virtualbox vagrant
```

## Docker

[Docker](https://docs.docker.com/) is a platform for developers and sysadmins to develop, ship, and run applications.

```
sudo snap install docker
```

Usage
```
snap info docker
name:      docker
summary:   The docker app deployment mechanism
publisher: Docker, Inc (docker-inc)
contact:   snappy-devel@lists.ubuntu.com
license:   unset
description: |
  Docker for snappy.
  
  This snap allows you to use the full capabilities of docker on snappy.
  
  In order to use 'docker build', 'docker save' and 'docker load', you need to place your dockerfile
  within $HOME. All files that you want docker to access to must be within this path.
  
  You may also use the 'docker-privilege' command to allow you to use 'docker run --privileged'.
  Because docker is unencumbered on snappy, it is recommended that you follow the Docker project's
  recommendations for using docker securely.
commands:
  - docker.compose
  - docker
  - docker.help
  - docker.machine
```

Add user to group docker 'reload shell'

```
sudo groupadd docker
sudo usermod -a -G docker $USER
sudo systemctl restart snap.docker.dockerd.service
```

## Terraform

Hashicorp [Terraform](https://www.terraform.io/) enables you to safely and predictably create, change, and improve infrastructure.

```bash
brew install terraform
```

## Ansible

[Ansible](https://www.ansible.com/) is an open source tool for automating configuration management, service orchestration, cloud provisioning and application deployment. Ansible is "agentless", using SSH to push changes from a single source to multiple remote resources. Commands can be invoked either ad hoc on the command line or via "playbooks" written in YAML.

```bash
brew install ansible
```

## AWS CLI

The AWS Command Line Interface (CLI) is a unified tool to manage your AWS services. With just one tool to download and configure, you can control multiple AWS services from the command line and automate them through scripts.

Install:

```bash
brew install awscli
```

See:
* https://docs.aws.amazon.com/cli/latest/userguide/installing.html
* https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html

## Google Cloud SDK

The Google Cloud SDK is a set of tools for the Google Cloud Platform and includes the `gcloud` command line tool.

```bash
sudo snap install google-cloud-sdk --classic
```

See:
* https://cloud.google.com/sdk/docs/quickstart-macos

## Kubernetes and Minikube

Kubectl is a command line interface for running commands against Kubernetes clusters.

```bash
brew install kubernetes-cli
```

https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-kubectl

It is also possible to use Minikube for your local Kubernetes environment. With Minikube you can configure another container, network-plugin, drivers, different kubernetes version, persistent volumes an other addons like ingress, efk, kube-dns, dashboard, etc.

See:
* https://kubernetes.io/docs/tasks/tools/install-kubectl/
* https://kubernetes.io/docs/setup/minikube/
* https://github.com/kubernetes/minikube/blob/master/docs/README.md
* https://github.com/telepresenceio/telepresence

* kubectl — kubectl is a command line interface for running commands against Kubernetes clusters
* kubectx — Fast way to switch between clusters and namespaces in kubectl!
* kubeval — Validate your Kubernetes configuration files, supports multiple Kubernetes versions
* helm — The Kubernetes Package Manager
* forge.sh — Define and deploy multi-container apps in Kubernetes, from source


## Other Apps

A list of productivity and office apps:

* [Boostnote](https://boostnote.io/#download) — To store code snippets and reference material for my development activities. Its open source, cross platform and works completely offline.
* [Simplenote](https://simplenote.com/) - “scratchpad”, for quick notes and thoughts
* [Chrome browser](https://www.google.com/chrome/) - Primary browser
* [Cerebro](https://github.com/KELiON/cerebro/releases) - Cerebro is an open source cross platform launcher similar to Spotlight and Alfred for Mac ``` sudo apt install gconf2 gconf-service libappindicator1 sudo dpkg -i cerebro_0.3.2_amd64.deb  ```
* [Mailspring](https://getmailspring.com/) - Mail client ``` sudo snap install mailspring ```
* [Slack](https://slack.com/) — For chat and engage with communities. ``` sudo snap install slack ```
* [vlc](https://www.videolan.org/vlc/#download) - Media player ``` sudo snap install vlc ```
* [Wireshark](https://www.wireshark.org/) — For network sniffing. ``` sudo apt install wireshark ```
* [GoogleDrive](https://www.google.com/drive/) - For sharing files.
* [Shutter](http://shutter-project.org/) — For taking screenshots ``` sudo apt install shutter ```
* [Insomnia](https://support.insomnia.rest/article/11-getting-started) - is a free cross-platform desktop application that takes the pain out of interacting with HTTP-based APIs. ``` sudo snap install insomnia ```
* [Stacer](https://github.com/oguzhaninan/Stacer) — Linux System Optimizer and Monitoring
* [Caffeine](https://launchpad.net/caffeine) — To keep my machine awake
* [safeincloud](https://www.safe-in-cloud.com/en/) - Password manager "Need play on linux"

Check also Command line utilities and Development tools: https://dev.to/brpaz/my-linux-development-environment-of-2018-ch7

